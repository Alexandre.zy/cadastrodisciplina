public class ControleDisciplina{
	//Atributo
	private ArrayList<Disciplina> listaDisciplinas;
	
	//Construtor
	ControleDisciplina(){
	listaDisciplinas = new ArrayList<Disciplina>();
	}
	
	//Métodos
	//Adicionar
	public void adicionarDisciplina(Disciplina disciplina){
		listaDisciplinas.add(disciplina);
	}
	//Remover
	public void removerDisciplina(Disciplina disciplina){
		listaDisciplinas.remove(disciplina);
}

	//Pesquisar Disciplina
	public void pesquisarDisciplina(String nomeDisciplina){
		for(Disciplina disciplina :listaDisciplinas ){
			if(disciplina.getNomeDisciplina().equalsIgnoreCase(NomeDisciplina)) return disciplina;
	}
		return null;
	}

	//Exibir lista de disciplinas
	public void exibirDisciplinas(){
		for(Disciplina disciplina : listaDisciplinas){
			System.out.println("Disciplina: "+disciplina.getNomeDisciplina());
			System.out.println("Professor: "+disciplina.getProfessor());
		}
	}
}
