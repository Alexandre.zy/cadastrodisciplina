mport java.util.ArrayList;
public class Disciplina{

//Atributos

	public ArrayList<Aluno> listaAlunos;
	private String Professor;
	private String nomeDisciplina;

//Construtores

	Disciplina (){
		this.listaAlunos = new ArrayList<Aluno>();	
	}
	Disciplina( String Professor, String nomeDisciplina){

		this.listaAlunos = new ArrayList<Aluno>();
		this.Professor = Professor;
		this.nomeDisciplina = nomeDisciplina;

	}
	
//Métodos

//Adicionar aluno
	public void adicionaAluno(String umNome){
		for(Aluno alunos :listaAlunos ){
                        if(alunos.getNome().equalsIgnoreCase(umNome)) return alunos;
        }
                return null;
        }


	public void setProfessor(String Professor){
		this.Professor = Professor;
	}
	public String getProfessor(){
		return this.Professor;
	}
	
	public void setNomeDisciplina(String nomeDisciplina){
		this.nomeDisciplina = nomeDisciplina;
	}
	public String getNomeDisciplina(){
		return this.nomeDisciplina;
	}
}
