public class Aluno{

//Atributos

	private String nome;
	private String matricula;

//Construtor
	Aluno(){
	}
	Aluno(String nome, String matricula){
		this.nome = nome;
		this.matricula = matricula;
	}
//Métodos
	public void setNome(String nome){
		this.nome = nome;
	}

	public String getNome(){
		return this.nome;
	}

	public void setMatricula(String matricula){
		this.matricula = matricula;
	}
	public String getMatricula(){
		return this.matricula;
	}
}

